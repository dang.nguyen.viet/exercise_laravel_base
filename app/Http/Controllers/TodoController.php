<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TodoController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        $id = Auth::user()->id;
        $todos = DB::table('todo_list')->where('user_id', '=', $id)->get()->all();
        return view('pages.todo-list', ['todos' => $todos]);
    }

    public function add(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required',
            'deadline' => 'required'
        ]);
        $user = Auth::getUser();
        DB::table('todo_list')->insert([
            'title' => htmlspecialchars($validated['title']),
            'note' => htmlspecialchars($request->note),
            'deadline' => htmlspecialchars($validated['deadline']),
            'user_id' => htmlspecialchars($user->id)
        ]);

        return redirect('/todo');
    }

    public function edit(Request $request, $id)
    {
        $data = [
            'title' => $request->title,
            'note' => $request->note,
            'deadline' => $request->deadline
        ];
        DB::table('todo_list')->where('id', '=', $id)->update($data);
        return redirect('/todo');
    }

    public function delete($id)
    {
        DB::table('todo_list')->delete($id);
        return redirect('/todo');
    }
}
