<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\VarDumper\VarDumper;

class UserController extends Controller
{
    public function __construct()
    {
    }

    public function loginIndex()
    {
        if (Auth::check())
            return redirect('/todo');
        else
            return view('pages.login');
    }

    public function login(Request $request)
    {
        $validated = $request->validate([
            'uname'   => 'required|exists:users,username',
            'psw' => 'required'
        ]);

        $user = DB::table('users')->where('username', '=', $validated['uname'])->get()->first();
        if (Hash::check($validated['psw'], $user->password)) {
            Auth::loginUsingId($user->id);
            return redirect('/todo');
        } else {
            dd('login fail');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }

    public function registerIndex()
    {
        return view('pages.register');
    }

    public function register(Request $request)
    {
        $validated = $request->validate([
            'uname'   => 'required|unique:users,username',
            'psw' => 'required|min:8|confirmed'
        ]);
        DB::table('users')->insert([
            'username' => $validated['uname'],
            'password' => Hash::make($validated['psw']),
        ]);
        return redirect('/login');
    }
}
