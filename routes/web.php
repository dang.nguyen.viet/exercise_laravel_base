<?php

use App\Http\Controllers\TodoController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/login', [UserController::class, 'loginIndex'])->name('login.index');
Route::post('/login', [UserController::class, 'login'])->name('login');
Route::get('/logout', [UserController::class, 'logout'])->name('logout');

Route::get('/register', [UserController::class, 'registerIndex'])->name('register.index');
Route::post('/register', [UserController::class, 'register'])->name('register');

Route::get('/todo', [TodoController::class, 'index'])->middleware('auth')->name('todo.index');
Route::get('/todo/add', function () {
    return
        view('pages.todo-add', ['data' => null]);;
})->middleware('auth')->name('todo.add.index');
Route::post('/todo/add', [TodoController::class, 'add'])->middleware('auth')->name('todo.add');
Route::post('/todo/edit/{id}', [TodoController::class, 'edit'])->middleware('auth')->name('todo.edit');
Route::get('/todo/delete/{id}', [TodoController::class, 'delete'])->middleware('auth')->name('todo.delete');
Route::get('/todo/edit/{id}', function ($id) {
    $data = DB::table('todo_list')->find($id);
    return view('pages.todo-add', ['data' => $data]);
})->middleware('auth')->name('todo.edit.index');
