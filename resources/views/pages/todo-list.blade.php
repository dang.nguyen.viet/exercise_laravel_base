@extends('app')
@section('title')
TodoList
@endsection
@section('content')
<div class="d-grid gap-2 col-9 mx-auto mt-4 ">
    <h1>Todo Page</h1>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Tên công việc</th>
                <th scope="col">Ghi chú</th>
                <th scope="col">Thời hạn</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            @foreach ($todos as $index=>$item)
            <tr>
                <th scope="row">{{$index+1}}</th>
                <td>{{$item->title}}</td>
                <td>{{$item->note}}</td>
                <td>{{$item->deadline}}</td>
                <td>
                    <a class="btn" href="{{ route('todo.edit.index',$item->id) }}">
                        <button type="button" class="btn btn-primary">Sửa</button>
                    </a>
                    <a class="btn" href="{{ route('todo.delete',$item->id) }}">
                        <button type="button" class="btn btn-secondary">Xóa</button>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="d-grid gap-2 col-4 mx-auto ">
    <a class="btn" href="{{ route('todo.add.index') }}">
        <button type="button" class="btn btn-primary">Thêm công việc</button>
    </a>
    <a class="btn" href="{{ route('logout') }}">
        <button type="button" class="btn btn-secondary">Đăng xuất</button>
    </a>
</div>
@endsection