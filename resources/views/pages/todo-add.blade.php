@extends('app')
@section('title')
Add Task
@endsection
@section('content')
<div class="d-grid gap-2 col-6 mx-auto mt-5 ">
    <form action="{{ $data ? route('todo.edit', ['id' => $data->id] ) : route('todo.add') }}" method="post">
        @csrf
        <div class="mb-3">
            <label for="title" class="form-label">Tên công việc(*)</label>
            <input type="text" name="title" id="title" class="form-control" value="{{$data ? $data->title : ''}}"
                required>
        </div>
        <div class="mb-3">
            <label for="note" class="form-label">Chú thích</label>
            <textarea rows="4" name="note" id="note" class="form-control">{{$data ? $data->note : ''}}</textarea>
        </div>
        <div class="mb-3">
            <label for="deadline" class="form-label">Thời hạn</label>
            <input type="datetime-local" name="deadline" id="deadline" class="form-control" value="{{$data?->deadline}}"
                required>
        </div>
        <button type="submit" class="btn btn-primary">
            <?= $data ? 'Sửa':'Thêm' ?>
        </button>
    </form>
</div>
@endsection