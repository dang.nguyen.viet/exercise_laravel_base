@extends('app')
@section('title')
Register
@endsection
@section('content')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="d-grid gap-2 col-6 mx-auto mt-5">
    <form action="" method="post">
        @csrf
        <div class="container">
            <div class="mb-3">
                <label for="uname" class="form-label"><b>Tên đăng nhập(*)</b></label>
                <input type="text" name="uname" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="psw" class="form-label"><b>Mật khẩu(*)</b></label>
                <input type="password" name="psw" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="psw_confirmation" class="form-label"><b>Xác nhận mật khẩu(*)</b></label>
                <input type="password" name="psw_confirmation" class="form-control" required>
            </div>
            <div class="d-grid gap-2 col-6 mx-auto mt-5">
                <button type="submit" class="btn btn-primary ">Đăng ký</button>
                <button class="btn"><a href="{{route('login.index')}}">Đăng nhập?</a></button>
            </div>
        </div>

    </form>
</div>
@endsection