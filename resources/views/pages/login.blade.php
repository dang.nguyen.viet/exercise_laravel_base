@extends('app')
@section('title', 'Login')
@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="d-grid gap-2 col-4 mx-auto mt-5 pt-3">
    <!-- Login Form -->
    <form action="{{route('login')}}" method="post">
        @csrf
        <div>
            <div class="mb-3">
                <label for="uname" class="form-label"><b>Tên đăng nhập(*)</b></label>
                <input type="text" name="uname" class="form-control" required>
            </div>
            <div class="mb-3">
                <label for="psw" class="form-label"><b>Mật khẩu(*){{ $errors->first('psw') }}</b></label>
                <input type="password" name="psw" class="form-control" required>
            </div>
        </div>

        <label>
            <input type="checkbox" checked="checked" name="remember"> Nhớ mật khẩu
        </label>
        <div class="d-grid gap-2 col-6 mx-auto mt-3">
            <button type="submit" class="btn btn-primary">Đăng nhập</button>
        </div>
        <div class="d-grid gap-2 col-6 mx-auto mt-3">
            <button type="button" class="btn"><a href="{{route('register.index')}}">Đăng ký?</a></button>
        </div>
    </form>
</div>
@endsection